package com.happyparrot.tarelka;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class SettingsTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void simpleSettingsTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_preferences)).perform(click());
        //onView(withId(R.id.tv_preferences)).check(matches(withText("Настройки")));

        onView(withId(R.id.b_preferences)).check(matches(withText("Изменить предпочтения")));
        onView(withId(R.id.b_preferences)).check(matches(isClickable()));

        onView(withId(R.id.b_aboutUs)).check(matches(withText("О нас")));
        onView(withId(R.id.b_aboutUs)).check(matches(isClickable()));

        onView(withId(R.id.b_support)).check(matches(withText("Написать в поддержку")));
        onView(withId(R.id.b_support)).check(matches(isClickable()));
    }

    @Test
    public void changePreferencesTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_preferences)).perform(click());

        onView(withId(R.id.b_preferences)).perform(click());
        onView(withId(R.id.cuisine_header)).check(matches(withText("Выберите любимую кухню")));

        onView(withText("Домашняя кухня")).check(matches(isDisplayed()));
        onView(withText("Домашняя кухня")).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_header)).check(matches(withText("Выберите тип диеты")));

        onView(withText("Постное")).check(matches(isDisplayed()));
        onView(withText("Постное")).perform(click());

        onView(withId(R.id.diet_next)).perform(click());
        onView(withId(R.id.ingredient_choice_header)).check(matches(withText("Исключить ингредиенты")));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText("Далее")).perform(click());
    }

    @Test
    public void changeManyCuisineTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_preferences)).perform(click());

        onView(withId(R.id.b_preferences)).perform(click());
        onView(withId(R.id.cuisine_header)).check(matches(withText("Выберите любимую кухню")));

        onView(withText("Грузинская кухня")).check(matches(isDisplayed()));
        onView(withText("Грузинская кухня")).perform(click());

        onView(withText("Домашняя кухня")).check(matches(isDisplayed()));
        onView(withText("Домашняя кухня")).perform(click());

        onView(withText("Японская кухня")).check(matches(isDisplayed()));
        onView(withText("Японская кухня")).perform(click());

        onView(withText("Русская кухня")).check(matches(isDisplayed()));
        onView(withText("Русская кухня")).perform(click());

        onView(withText("Украинская кухня")).check(matches(isDisplayed()));
        onView(withText("Украинская кухня")).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_header)).check(matches(withText("Выберите тип диеты")));

        onView(withId(R.id.diet_next)).perform(click());
        onView(withId(R.id.ingredient_choice_header)).check(matches(withText("Исключить ингредиенты")));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText("Далее")).perform(click());
    }

    @Test
    public void changeManyDietTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_preferences)).perform(click());

        onView(withId(R.id.b_preferences)).perform(click());
        onView(withId(R.id.cuisine_header)).check(matches(withText("Выберите любимую кухню")));

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_header)).check(matches(withText("Выберите тип диеты")));

        onView(withText("Низкокалорийное")).check(matches(isDisplayed()));
        onView(withText("Низкокалорийное")).perform(click());

        onView(withText("Диетическое")).check(matches(isDisplayed()));
        onView(withText("Диетическое")).perform(click());

        onView(withText("Правильное питание")).check(matches(isDisplayed()));
        onView(withText("Правильное питание")).perform(click());

        onView(withText("Постное")).check(matches(isDisplayed()));
        onView(withText("Постное")).perform(click());

        onView(withText("Вегетарианское")).check(matches(isDisplayed()));
        onView(withText("Вегетарианское")).perform(click());

        onView(withId(R.id.diet_next)).perform(click());
        onView(withId(R.id.ingredient_choice_header)).check(matches(withText("Исключить ингредиенты")));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText("Далее")).perform(click());
    }

    @Test
    public void changeManyPreferencesTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_preferences)).perform(click());

        onView(withId(R.id.b_preferences)).perform(click());
        onView(withId(R.id.cuisine_header)).check(matches(withText("Выберите любимую кухню")));

        onView(withText("Грузинская кухня")).check(matches(isDisplayed()));
        onView(withText("Грузинская кухня")).perform(click());

        onView(withText("Домашняя кухня")).check(matches(isDisplayed()));
        onView(withText("Домашняя кухня")).perform(click());

        onView(withText("Японская кухня")).check(matches(isDisplayed()));
        onView(withText("Японская кухня")).perform(click());

        onView(withText("Русская кухня")).check(matches(isDisplayed()));
        onView(withText("Русская кухня")).perform(click());

        onView(withText("Украинская кухня")).check(matches(isDisplayed()));
        onView(withText("Украинская кухня")).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_header)).check(matches(withText("Выберите тип диеты")));

        onView(withText("Низкокалорийное")).check(matches(isDisplayed()));
        onView(withText("Низкокалорийное")).perform(click());

        onView(withText("Диетическое")).check(matches(isDisplayed()));
        onView(withText("Диетическое")).perform(click());

        onView(withText("Правильное питание")).check(matches(isDisplayed()));
        onView(withText("Правильное питание")).perform(click());

        onView(withText("Постное")).check(matches(isDisplayed()));
        onView(withText("Постное")).perform(click());

        onView(withText("Вегетарианское")).check(matches(isDisplayed()));
        onView(withText("Вегетарианское")).perform(click());

        onView(withId(R.id.diet_next)).perform(click());
        onView(withId(R.id.ingredient_choice_header)).check(matches(withText("Исключить ингредиенты")));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText("Далее")).perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
