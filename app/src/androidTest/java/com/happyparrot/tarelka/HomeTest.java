package com.happyparrot.tarelka;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.action.ViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HomeTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void simpleHomeTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_home)).perform(click());

        onView(withText("Завтрак")).check(matches(isDisplayed()));
        onView(withText("Обед")).check(matches(isDisplayed()));
        onView(withText("Ужин")).check(matches(isDisplayed()));

        onView(withId(R.id.previous_day_btn)).check(matches(isDisplayed()));

        onView(withId(R.id.next_day_btn)).check(matches(isDisplayed()));
        onView(withId(R.id.next_day_btn)).check(matches(isClickable()));
    }

    @Test
    public void nextDaysTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_home)).perform(click());

        for (int i = 0; i < 14; i++) {
            onView(withId(R.id.next_day_btn)).check(matches(isClickable()));
            onView(withId(R.id.next_day_btn)).perform(click());
        }
    }

    @Test
    public void scrollDaysTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_home)).perform(click());

        for (int i = 0; i < 5; i++) {
            onView(withId(R.id.next_day_btn)).check(matches(isClickable()));
            onView(withId(R.id.next_day_btn)).perform(click());
        }

        for (int i = 0; i < 5; i++) {
            onView(withId(R.id.previous_day_btn)).check(matches(isClickable()));
            onView(withId(R.id.previous_day_btn)).perform(click());
        }

        for (int i = 0; i < 10; i++) {
            onView(withId(R.id.next_day_btn)).check(matches(isClickable()));
            onView(withId(R.id.next_day_btn)).perform(click());
        }

        for (int i = 0; i < 4; i++) {
            onView(withId(R.id.previous_day_btn)).check(matches(isClickable()));
            onView(withId(R.id.previous_day_btn)).perform(click());
        }
    }

    @Test
    public void recipeTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_home)).perform(click());

        onView(withId(R.id.plate_breakfast)).perform(click());
        onView(withId(R.id.recipe_carbohydrates)).check(matches(withText("Углеводы")));
        onView(withId(R.id.recipe_fats)).check(matches(withText("Жиры")));
        onView(withId(R.id.recipe_proteins)).check(matches(withText("Белки")));

        onView(withText("Ингредиенты")).check(matches(isDisplayed()));
        onView(withId(R.id.prepare)).check(matches(isDisplayed()));
        onView(withId(R.id.prepare)).check(matches(isClickable()));
    }

    @Test
    public void prepareRecipeTest() {
        onView(
                allOf(childAtPosition(
                        allOf(withId(android.R.id.content),
                                childAtPosition(
                                        withId(R.id.action_bar_root),
                                        1)),
                        0),
                        isDisplayed())).perform(click());

        onView(withId(R.id.cuisine_next)).perform(click());
        onView(withId(R.id.diet_next)).perform(click());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withText("Далее")).perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.navigation_home)).perform(click());

        onView(withId(R.id.plate_breakfast)).perform(click());

        onView(withId(R.id.prepare)).check(matches(isClickable()));
        onView(withId(R.id.prepare)).perform(click());

        onView(withText("Ингредиенты")).check(matches(isDisplayed()));
        onView(withText("Способ приготовления")).perform(ViewActions.scrollTo()).check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
