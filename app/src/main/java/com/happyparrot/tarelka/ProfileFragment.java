package com.happyparrot.tarelka;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.happyparrot.tarelka.menu.MenuController;

import javax.xml.datatype.Duration;

public class ProfileFragment extends Fragment {
    private TextView proteins;
    private TextView fats;
    private TextView carbohydrates;
    private TextView calories;


    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        proteins = view.findViewById(R.id.textView_proteins_value);
        fats = view.findViewById(R.id.textView_fats_value);
        carbohydrates = view.findViewById(R.id.textView_carbohydrates_value);
        calories = view.findViewById(R.id.textView_calories_value);

        initPFCC();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initPFCC();
    }

    private void initPFCC() {
        proteins.setText(String.format("%.2f", MenuController.getProteins()));
        fats.setText(String.format("%.2f", MenuController.getFats()));
        carbohydrates.setText(String.format("%.2f", MenuController.getCarbons()));
        calories.setText(String.format("%.2f", MenuController.getCalories()));
    }

}
