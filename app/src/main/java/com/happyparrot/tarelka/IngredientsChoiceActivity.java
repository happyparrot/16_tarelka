package com.happyparrot.tarelka;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.happyparrot.tarelka.menu.CantCreateMenuException;
import com.happyparrot.tarelka.menu.MenuController;
import com.happyparrot.tarelka.user.Profile;

import java.util.ArrayList;

public class IngredientsChoiceActivity extends AppCompatActivity {
    private Button next;
    private Button back;
    private ListView excludedIngredients;
    EditText ingredient_enter_field;
    ExcludedIngredientsListAdapter adapter;

    ArrayList<String> ingredients = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients_choice);
        next = findViewById(R.id.ingredients_choice_next);
        back = findViewById(R.id.ingredients_choice_back);
        excludedIngredients = findViewById(R.id.excluded_ingredients);
        ingredient_enter_field = findViewById(R.id.enter_excluded_ingredient);
        ingredient_enter_field.setInputType(InputType.TYPE_CLASS_TEXT);

        ingredients = Profile.getIngredients();

        adapter = new ExcludedIngredientsListAdapter(this, R.layout.listview_excluded_ingredients, ingredients);
        excludedIngredients.setAdapter(adapter);

        ingredient_enter_field.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    addIngredient();
                    return true;
                }
                return false;
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile.setIngredients(ingredients);
                finish();
                try {
                    MenuController.updateMenu();
                } catch (CantCreateMenuException e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
                startActivity(new Intent(IngredientsChoiceActivity.this, MainActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(IngredientsChoiceActivity.this, DietChoiceActivity.class));
            }
        });
    }

    private void addIngredient() {
        if (!ingredients.contains(ingredient_enter_field.getText().toString())) {
            ingredients.add(ingredient_enter_field.getText().toString());
            adapter.notifyDataSetChanged();
            ingredient_enter_field.setText("");
        }
    }

    public void removeIngredient(View view) {
        TextView t = view.findViewById(R.id.excluded_ingredient_name);
        ingredients.remove(t.getText().toString());
        adapter.notifyDataSetChanged();
    }
}
