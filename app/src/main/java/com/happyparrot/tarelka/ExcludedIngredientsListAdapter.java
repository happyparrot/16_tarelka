package com.happyparrot.tarelka;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.happyparrot.tarelka.dish.Product;

import java.util.ArrayList;

public class ExcludedIngredientsListAdapter extends ArrayAdapter<String> {
    Context context;
    int resource;

    public ExcludedIngredientsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);
        TextView name = convertView.findViewById(R.id.excluded_ingredient_name);
        name.setText(getItem(position));
        return convertView;
    }
}
