package com.happyparrot.tarelka.menu;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.happyparrot.tarelka.dish.Dish;
import com.happyparrot.tarelka.dish.PFCC;
import com.happyparrot.tarelka.dish.Period;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * works with 14 days menu (for front)
 */
public class MenuController {
    static MenuForWeek menu;
    static List<Dish> allDishes;

    /**
     * Инициализирует и создает меню
     * Вызвать только один раз
     * @param dishes
     * @throws CantCreateMenuException
     */
    public static void initialize(List<Dish> dishes) throws CantCreateMenuException {
        allDishes = dishes;
        menu = new MenuForWeek(allDishes);
    }

    /**
     * Обновляет меню согласно предпочтениям
     * @throws CantCreateMenuException
     */
    public static void updateMenu() throws CantCreateMenuException {
        menu.refreshAppropriateDishesAndMenu(allDishes);
    }

    /**
     * Проверяется дата первого блюда и текущая дата
     * Если они не совпадают, то меню обновляется до актуальной даты
     */
    public static void checkRelevance() throws CantCreateMenuException {
        Calendar c = Calendar.getInstance();
        Calendar c2 = menu.getOneDayMenus().get(0).getCalendar();
        c.setTime(new Date());
        while (c.get(Calendar.YEAR) != c2.get(Calendar.YEAR)
                && c.get(Calendar.DAY_OF_YEAR) != c.get(Calendar.DAY_OF_YEAR)) {
            menu.addOneDayMenu(true);
        }
    }

    public static OneDayMenu getMenuForDay(int dayNumber) {
        return menu.getOneDayMenus().get(dayNumber);
    }

    public static List<OneDayMenu> getMenuForWeek() {
        List<OneDayMenu> menuForWeek = new ArrayList<>();

        for (int i = 0; i < 7; ++i) {
            menuForWeek.add(menu.getOneDayMenus().get(i));
        }
        return menuForWeek;
    }

    public static List<OneDayMenu> getWholeMenu() {
        return menu.getOneDayMenus();
    }

    public static Dish updateDish(int dayNumber, Period per) {
        return menu.updateDish(dayNumber, per);
    }

    public static float getCalories() {
        return menu.getWeeklyPFCC().getCalories();
    }

    public static float getCarbons() {
        return menu.getWeeklyPFCC().getCarbs();
    }

    public static float getFats() {
        return menu.getWeeklyPFCC().getFats();
    }

    public static float getProteins() {
        return menu.getWeeklyPFCC().getProteins();
    }

    public static boolean menuExists() {
        return menu != null;
    }

}
