package com.happyparrot.tarelka.menu;

import com.happyparrot.tarelka.dish.Dish;
import com.happyparrot.tarelka.dish.PFCC;

import java.util.Calendar;
import java.util.Date;

/**
 * menu for a single day
 */
public class OneDayMenu {
    Calendar calendar = Calendar.getInstance();
    Dish breakfast;
    Dish dinner;
    Dish supper;

    OneDayMenu (Date date, Dish breakfast, Dish dinner, Dish supper) {
        calendar.setTime(date);
        this.breakfast = breakfast;
        this.dinner = dinner;
        this.supper = supper;
    }

    private String convertIntToDayOfWeek(int a) {
        switch (a) {
            case 1:
                return "воскресенье";
            case 2:
                return "понедельник";
            case 3:
                return "вторник";
            case 4:
                return "среда";
            case 5:
                return "четверг";
            case 6:
                return "пятница";
            case 7:
                return "суббота";
            default:
                return "Oh...";
        }
    }

    private String convertIntToMonth(int a) {
        switch (a) {
            case 0:
                return "января";
            case 1:
                return "февраля";
            case 2:
                return "марта";
            case 3:
                return "апреля";
            case 4:
                return "мая";
            case 5:
                return "июня";
            case 6:
                return "июля";
            case 7:
                return "августа";
            case 8:
                return "сентября";
            case 9:
                return "октября";
            case 10:
                return "ноября";
            case 11:
                return "декабря";
            default:
                return "No...";
        }
    }

    public void setBreakfast(Dish breakfast) {
        this.breakfast = breakfast;
    }

    public void setDinner(Dish dinner) {
        this.dinner = dinner;
    }

    public void setSupper(Dish supper) {
        this.supper = supper;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    public Dish getBreakfast() {
        return breakfast;
    }

    public Dish getDinner() {
        return dinner;
    }

    public Dish getSupper() {
        return supper;
    }

    public PFCC getDailyPFCC() {
        return PFCC.sum(PFCC.sum(breakfast.getPffc(), dinner.getPffc()), supper.getPffc());
    }

    public String getDate() {
        return calendar.get(Calendar.DAY_OF_MONTH) + " " + convertIntToMonth(calendar.get(Calendar.MONTH)) + ", " + convertIntToDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
    }
}
