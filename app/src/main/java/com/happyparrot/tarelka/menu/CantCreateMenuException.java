package com.happyparrot.tarelka.menu;

public class CantCreateMenuException extends Exception{
    public CantCreateMenuException(String message) {
        super(message);
    }
}
