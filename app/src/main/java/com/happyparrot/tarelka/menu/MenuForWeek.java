package com.happyparrot.tarelka.menu;

import com.happyparrot.tarelka.dish.Cuisine;
import com.happyparrot.tarelka.dish.Diet;
import com.happyparrot.tarelka.dish.Dish;
import com.happyparrot.tarelka.dish.PFCC;
import com.happyparrot.tarelka.dish.Period;
import com.happyparrot.tarelka.dish.Product;
import com.happyparrot.tarelka.user.Profile;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MenuForWeek {

    Random r;
    List<OneDayMenu> oneDayMenus = new ArrayList<>(); //Само меню

    List<Dish> appropriateDishes = new ArrayList<>(); //Блюда, подходящие под предпочтения пользователя
    private List<Dish> appd, br, din, sup; //Вспомогательные штуки, чтобы обновлять блюда, которые мы вычеркивали
    List<Dish> breakfasts = new ArrayList<>(); //оставшиеся завтраки
    List<Dish> dinners = new ArrayList<>(); //оставшиеся обеды
    List<Dish> suppers = new ArrayList<>(); //оставшиеся ужины


    MenuForWeek(List<Dish> allDishes) throws CantCreateMenuException {
        r = new Random();
        refreshAppropriateDishesAndMenu(allDishes);
    }

    /**
     * Обновляем блюда согласно предпочтениям
     * Так как при обновлении предпочтений меню становится неактуальным, обновляется и само меню
     *
     * @param allDishes
     */
    public void refreshAppropriateDishesAndMenu(List<Dish> allDishes) throws CantCreateMenuException {
        setAppropriateDishes(allDishes);
        separateDishes();
        if (br.size() < 1 || din.size() < 1 || sup.size() < 1)
            throw new CantCreateMenuException("В базе недостаточно блюд, чтобы составить меню на неделю.\n" +
                    "Попробуйте сократить количество настроек пользователя или написать в поддержку.");
        for (int i = 0; i < 14; ++i) {
            if (i == 0)
                addOneDayMenu(false);
            else
                addOneDayMenu(true);
        }
    }

    /**
     * Выбираются только те блюда, которые удовлетворяют предпочтениям пользователя
     *
     * @param allDishes
     */
    private void setAppropriateDishes(List<Dish> allDishes) {
        Boolean f = true;
        for (Dish d : allDishes) {
            f = true;
            //Проверяем на отсутствие запрещенных веществ
            for (Product p : d.getIngredients())
                if (containsIngredient(p.getName())) {
                    f = false;
                    break;
                }
            if (!f && Profile.getIngredients().size() != 0)
                continue;

            //Хотя бы одна кухня есть
            f = false;
            for (Cuisine c : Profile.getCuisine())
                if (d.getCuisine().contains(c))
                    f = true;
            if (!f && Profile.getCuisine().size() != 0)
                continue;

            //Хотя бы одна диета есть
            f = false;
            for (Diet diet : Profile.getDiet())
                if (d.getDiet().contains(diet))
                    f = true;
            if (!f && Profile.getDiet().size() != 0)
                continue;

            appropriateDishes.add(d);
        }
    }

    private boolean containsIngredient(String ingredient) {
        for (String s : Profile.getIngredients()) {
            if ((s.toLowerCase()).equals(ingredient.toLowerCase()))
                return true;
        }
        return false;
    }

    /**
     * Блюда разделяются на завтраки обеды и ужины (они могут пересекаться)
     */
    private void separateDishes() {
        for (Dish dish : appropriateDishes) {
            if (dish.getPeriod().contains(Period.BREAKFAST))
                breakfasts.add(dish);
            if (dish.getPeriod().contains(Period.DINNER))
                dinners.add(dish);
            if (dish.getPeriod().contains(Period.SUPPER))
                suppers.add(dish);
        }

        appd = new ArrayList<>(appropriateDishes);
        br = new ArrayList<>(breakfasts);
        din = new ArrayList<>(dinners);
        sup = new ArrayList<>(suppers);
    }

    /**
     * Обновляет списки блюд, все блюда, соответствующие предпочтениям, теперь доступны
     * Необходимо в ситуациях, когда не хватает блюд для составления разнообразного меню
     */
    private void refresh() {
        breakfasts = new ArrayList<>(br);
        dinners = new ArrayList<>(din);
        suppers = new ArrayList<>(sup);
        appropriateDishes = new ArrayList<>(appd);
    }

    /**
     * Проверка на то, хватает ли блюд, чтобы добавить еще один день
     *
     * @return
     */
    private boolean fastCheck() {
        return appropriateDishes.size() >= 3 && breakfasts.size() >= 1 && dinners.size() >= 1 && suppers.size() >= 1;
    }

    /**
     * Удалить блюдо из всех списков (необходимо, когда мы добавляем это блюдо в меню)
     *
     * @param dish
     */
    private void reserveDish(Dish dish) {
        appropriateDishes.remove(dish);
        breakfasts.remove(dish);
        dinners.remove(dish);
        suppers.remove(dish);
    }

    /**
     * Возвращает блюдо в списки, если его там нет (необходимо, когда день очищается)
     *
     * @param dish
     */
    public void returnDish(Dish dish) {
        if (appropriateDishes.contains(dish))
            return;
        appropriateDishes.add(dish);
        if (dish.getPeriod().contains(Period.BREAKFAST))
            breakfasts.add(dish);
        if (dish.getPeriod().contains(Period.DINNER))
            dinners.add(dish);
        if (dish.getPeriod().contains(Period.SUPPER))
            suppers.add(dish);
    }

    /**
     * Если в меню уже есть 14 дней, то удаляется первый день
     * При этом все блюда из первого дня возвращаются в списки
     */
    private void deleteFirstDay() {
        if (oneDayMenus.size() > 13) {
            returnDish(oneDayMenus.get(0).getBreakfast());
            returnDish(oneDayMenus.get(0).getDinner());
            returnDish(oneDayMenus.get(0).getSupper());
            oneDayMenus.remove(0);
        }
    }

    /**
     * Удаляет лишний день из меню
     * Пытается добавить новый день или с новыми блюдами или с повторяющимися (если не хватает блюд)
     * В случае если все равно не получается выбрать три блюда, бросается исключение
     *
     * @throws CantCreateMenuException
     */
    public void addOneDayMenu(boolean updateDateFlag) throws CantCreateMenuException {
        deleteFirstDay();
        OneDayMenu menu;
        Dish b, d, s;
        if (fastCheck()) {
            int i = r.nextInt(breakfasts.size());
            b = breakfasts.get(i);
            reserveDish(breakfasts.get(i));

            i = r.nextInt(dinners.size());
            d = dinners.get(i);
            reserveDish(dinners.get(i));

            i = r.nextInt(suppers.size());
            s = suppers.get(i);
            reserveDish(suppers.get(i));

            Calendar c;

            if (oneDayMenus.isEmpty() || !updateDateFlag) {
                Date date = new Date();
                c = Calendar.getInstance();
                c.setTime(date);
            } else {
                c = (Calendar) oneDayMenus.get(oneDayMenus.size() - 1).getCalendar().clone();
                c.add(Calendar.DATE, 1);
            }
            menu = new OneDayMenu(c.getTime(), b, d, s);
            oneDayMenus.add(menu);
            return;
        }

        refresh();
        addOneDayMenu(true);
    }

    public Dish updateDish(int dayNumber, Period per) {
        Dish d;
        switch (per) {
            case BREAKFAST:
                if (breakfasts.size() > 0) {
                    int i = r.nextInt(breakfasts.size());
                    d = breakfasts.get(i);
                    returnDish(oneDayMenus.get(dayNumber).getBreakfast());
                    oneDayMenus.get(dayNumber).setBreakfast(d);
                    reserveDish(d);
                }
                return oneDayMenus.get(dayNumber).getBreakfast();
            case DINNER:
                if (dinners.size() > 0) {
                    int i = r.nextInt(dinners.size());
                    d = dinners.get(i);
                    returnDish(oneDayMenus.get(dayNumber).getDinner());
                    oneDayMenus.get(dayNumber).setDinner(d);
                    reserveDish(d);
                }
                return oneDayMenus.get(dayNumber).getDinner();
            case SUPPER:
                if (suppers.size() > 0) {
                    int i = r.nextInt(suppers.size());
                    d = suppers.get(i);
                    returnDish(oneDayMenus.get(dayNumber).getSupper());
                    oneDayMenus.get(dayNumber).setSupper(d);
                    reserveDish(d);
                }
                return oneDayMenus.get(dayNumber).getSupper();
        }
        return null;
    }

    public List<OneDayMenu> getOneDayMenus() {
        return oneDayMenus;
    }

    public List<Dish> getAppropriateDishes() {
        return appropriateDishes;
    }

    public List<Dish> getBreakfasts() {
        return breakfasts;
    }

    public List<Dish> getDinners() {
        return dinners;
    }

    public List<Dish> getSuppers() {
        return suppers;
    }

    /**
     * БЖУк за следующие 14 дней
     *
     * @return
     */
    public PFCC getWeeklyPFCC() {
        PFCC sum = new PFCC(0, 0, 0, 0);
        for (OneDayMenu temp : oneDayMenus) {
            sum = PFCC.sum(sum, temp.getDailyPFCC());
        }
        return sum;
    }

}
