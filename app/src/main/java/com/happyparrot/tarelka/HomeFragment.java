package com.happyparrot.tarelka;


import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.happyparrot.tarelka.dish.Period;
import com.happyparrot.tarelka.menu.MenuController;

import java.io.IOException;
import java.io.InputStream;

public class HomeFragment extends Fragment {

    //OneDayMenu menu;
    static int CURRENT_DAY_INDEX = 0;
    static int MEAL_INDEX = 0;

    //private Dish breakfast;
    private ImageView breakfast_image;
    private TextView breakfast_name;
    private ImageView breakfast_refresh;
    private RelativeLayout breakfast_plate;

    //private Dish dinner;
    private ImageView dinner_image;
    private TextView dinner_name;
    private ImageView dinner_refresh;
    private RelativeLayout dinner_plate;

    //private Dish supper;
    private ImageView supper_image;
    private TextView supper_name;
    private ImageView supper_refresh;
    private RelativeLayout supper_plate;

    private TextView date;
    private ImageButton back;
    private ImageButton forward;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        date = view.findViewById(R.id.home_date);
        back = view.findViewById(R.id.previous_day_btn);
        forward = view.findViewById(R.id.next_day_btn);

        breakfast_refresh = view.findViewById(R.id.refresh_breakfast);
        breakfast_image = view.findViewById(R.id.image_breakfast);
        breakfast_name = view.findViewById(R.id.breakfast_dish_name);
        breakfast_plate = view.findViewById(R.id.plate_breakfast);

        dinner_refresh = view.findViewById(R.id.refresh_dinner);
        dinner_image = view.findViewById(R.id.image_dinner);
        dinner_name = view.findViewById(R.id.dinner_dish_name);
        dinner_plate = view.findViewById(R.id.plate_dinner);

        supper_refresh = view.findViewById(R.id.refresh_supper);
        supper_image = view.findViewById(R.id.image_supper);
        supper_name = view.findViewById(R.id.supper_dish_name);
        supper_plate = view.findViewById(R.id.plate_supper);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previousDay();
            }
        });

        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextDay();
            }
        });

        breakfast_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshBreakfast();
            }
        });
        dinner_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshDinner();
            }
        });
        supper_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshSupper();
            }
        });

        breakfast_plate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MEAL_INDEX = 0;
                startActivity(new Intent(getActivity(), RecipeActivity.class));
            }
        });
        dinner_plate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MEAL_INDEX = 1;
                startActivity(new Intent(getActivity(), RecipeActivity.class));
            }
        });
        supper_plate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MEAL_INDEX = 2;
                startActivity(new Intent(getActivity(), RecipeActivity.class));
            }
        });

        initializeMenu();

        return view;
    }

    private void initializeMenu() {
        breakfast_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getBreakfast().getName());
        breakfast_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getBreakfast().getImagePath()));

        dinner_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getDinner().getName());
        dinner_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getDinner().getImagePath()));

        supper_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getSupper().getName());
        supper_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getSupper().getImagePath()));

        date.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getDate());
    }

    private void refreshBreakfast() {
        MEAL_INDEX = 0;
        MenuController.updateDish(CURRENT_DAY_INDEX, Period.BREAKFAST);
        breakfast_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getBreakfast().getName());
        breakfast_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getBreakfast().getImagePath()));
    }

    private void refreshDinner() {
        MEAL_INDEX = 1;
        MenuController.updateDish(CURRENT_DAY_INDEX, Period.DINNER);
        dinner_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getDinner().getName());
        dinner_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getDinner().getImagePath()));
    }

    private void refreshSupper() {
        MEAL_INDEX = 2;
        MenuController.updateDish(CURRENT_DAY_INDEX, Period.SUPPER);
        supper_name.setText(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getSupper().getName());
        supper_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(CURRENT_DAY_INDEX).getSupper().getImagePath()));
    }

    private Bitmap getBitmapFromAssets(String fileName) {
        AssetManager am = getActivity().getAssets();
        InputStream is = null;
        try {
            is = am.open("images/" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }


    private void nextDay() {
        if (HomeFragment.CURRENT_DAY_INDEX < 13) {
            HomeFragment.CURRENT_DAY_INDEX++;
            initializeMenu();
        } else {
            //show error message
        }
    }

    private void previousDay() {
        if (HomeFragment.CURRENT_DAY_INDEX > 0) {
            HomeFragment.CURRENT_DAY_INDEX--;
            initializeMenu();
        } else {
            //show error message
        }
    }

}
