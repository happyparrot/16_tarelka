package com.happyparrot.tarelka;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.happyparrot.tarelka.dish.Product;
import com.happyparrot.tarelka.menu.MenuController;

public class PrepareRecipeActivity extends AppCompatActivity {

    ArrayList<Product> productArrayList;
    LinearLayout ingredientsList;
    private ImageView recipe_image;
    private TextView recipe_name;
    private TextView recipe_time;
    private TextView recipe_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepare_recipe);
        recipe_name = findViewById(R.id.recipe_name);
        recipe_image = findViewById(R.id.recipe_image);
        recipe_time = findViewById(R.id.recipe_time);
        recipe_text = findViewById(R.id.recipe_textView);
        recipe_text.setMovementMethod(new ScrollingMovementMethod());

        ingredientsList = findViewById(R.id.prepare_recipe_ingredients);

        initRecipe();
        for (int i = 0; i < productArrayList.size(); i++) {
            setSampleIngredients(productArrayList.get(i));
        }
    }

    private void setSampleIngredients(Product product) {
        View ingredientView = LayoutInflater.from(this).inflate(R.layout.listview_ingredients_prepare, null);
        TextView name = ingredientView.findViewById(R.id.prepare_ingredient_name);
        TextView count = ingredientView.findViewById(R.id.prepare_ingredient_count);
        name.setText(product.getName());
        count.setText((product.getAmount() == 0 ? "" : (product.getAmount() + " ")) + product.getUnit());
        ingredientsList.addView(ingredientView);
    }

    void initRecipe() {
        productArrayList = new ArrayList<>();
        switch (HomeFragment.MEAL_INDEX) {
            case 0:
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getName());
                recipe_text.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getRecipe());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getIngredients();
                break;
            case 1:
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getName());
                recipe_text.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getRecipe());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getIngredients();
                break;
            case 2:
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getName());
                recipe_text.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getRecipe());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getIngredients();
                break;
        }

    }

    private Bitmap getBitmapFromAssets(String fileName) {
        AssetManager am = getAssets();
        InputStream is = null;
        try {
            is = am.open("images/" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }

    public void closeActivity(View view) {
        finish();
    }
}
