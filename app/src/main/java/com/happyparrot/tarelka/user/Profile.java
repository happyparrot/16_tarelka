package com.happyparrot.tarelka.user;

import com.happyparrot.tarelka.dish.*;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Profile {
    static EnumSet<Cuisine> cuisine = EnumSet.noneOf(Cuisine.class);
    static EnumSet<Diet> diet = EnumSet.noneOf(Diet.class);
    static ArrayList<String> ingredients = new ArrayList<>();

    public static EnumSet<Cuisine> getCuisine() {
        return cuisine;
    }

    public static void setCuisine(EnumSet<Cuisine> c) {
        cuisine = c;
    }

    public static void setCuisine(Set<String> cuis) {
        cuisine = EnumSet.noneOf(Cuisine.class);
        for (String s : cuis) {
            if (MapsForSets.cuis.containsKey(s))
                cuisine.add(MapsForSets.cuis.get(s));
        }
    }

    public static Set<String> getCuisines() {
        Set<String> cuis = new HashSet<>();
        for (Cuisine c : cuisine) {
            cuis.add(MapsForSets.cuisReverse.get(c));
        }
        return cuis;
    }

    public static EnumSet<Diet> getDiet() {
        return diet;
    }

    public static void setDiet(EnumSet<Diet> d) {
        diet = d;
    }

    public static void setDiet(Set<String> d) {
        diet = EnumSet.noneOf(Diet.class);
        for (String s : d) {
            if (MapsForSets.diet.containsKey(s))
                diet.add(MapsForSets.diet.get(s));
        }
    }

    public static Set<String> getDiets() {
        Set<String> dt = new HashSet<>();
        for (Diet d : diet) {
            dt.add(MapsForSets.diet_reverse.get(d));
        }
        return dt;
    }

    public static ArrayList<String> getIngredients() {
        return ingredients;
    }

    public static void setIngredients(ArrayList<String> i) {
        ingredients = new ArrayList<>(i);
    }
}
