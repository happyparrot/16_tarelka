package com.happyparrot.tarelka;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.happyparrot.tarelka.menu.CantCreateMenuException;
import com.happyparrot.tarelka.menu.MenuController;
import com.happyparrot.tarelka.user.Profile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DietChoiceActivity extends AppCompatActivity {
    private Button next;
    private Button back;

    Set<String> diets = new HashSet<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diet);
        next = findViewById(R.id.diet_next);
        back = findViewById(R.id.diet_back);

        diets = Profile.getDiets();
        initButtons();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile.setDiet(diets);
                finish();
                try {
                    MenuController.updateMenu();
                } catch (CantCreateMenuException e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
                startActivity(new Intent(DietChoiceActivity.this, IngredientsChoiceActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(DietChoiceActivity.this, CuisineChoiceActivity.class));
            }
        });
    }

    public void addDiet(View view) {
        Button b = (Button) view;
        //Если выбрали любую диету, очищаем множество
//        if (((String) b.getText()).equals(getString(R.string.diet_default))) {
//            diets.clear();
//        }
        //Если кнопка нажата повторно, то есть мы хотим убрать диету из множества
        if (diets.contains((String) b.getText())) {
            diets.remove((String) b.getText());
            changeButtonState(b,false);
        }
        //Если кнопка нажата впервые, хотим добавить диету
        else {
            diets.add((String) b.getText());
            changeButtonState(b,true);
        }
    }

    private void changeButtonState(Button b, boolean activate){
        if(activate)
        {
            b.setTextColor(getResources().getColor(R.color.colorPrimary));
            b.setBackground(getResources().getDrawable(R.drawable.btn_bg_filled));
        }
        else {
            b.setTextColor(getResources().getColor(R.color.colorWhite));
            b.setBackground(getResources().getDrawable(R.drawable.btn_bg_contour));
        }
    }


    private void initButtons() {
        View rootView = findViewById(R.id.diet_choice);
        ArrayList<View> touchables = rootView.getTouchables();
        for (View v : touchables) {
            if (v instanceof Button && diets.contains(((Button) v).getText().toString())) {
                changeButtonState((Button) v, true);
            }
        }
    }
}
