package com.happyparrot.tarelka;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.happyparrot.tarelka.menu.CantCreateMenuException;
import com.happyparrot.tarelka.menu.MenuController;
import com.happyparrot.tarelka.user.Profile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CuisineChoiceActivity extends AppCompatActivity {

    Set<String> cuisines = new HashSet<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuisine);

        Button next = findViewById(R.id.cuisine_next);
        Button back = findViewById(R.id.cuisine_back);

        cuisines = Profile.getCuisines();
        initButtons();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile.setCuisine(cuisines);
                finish();
                try {
                    MenuController.updateMenu();
                } catch (CantCreateMenuException e) {
                    Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                }
                startActivity(new Intent(CuisineChoiceActivity.this, DietChoiceActivity.class));
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(CuisineChoiceActivity.this, MainActivity.class));
            }
        });
    }

    public void addCuisine(View view) {
        Button b = (Button) view;
        //Если выбрали любую кузню, очищаем множество
//        if (((String) b.getText()).equals(getString(R.string.cuisine_default))) {
//            cuisines.clear();
//        }
        //Если кнопка нажата повторно, то есть мы хотим убрать кухню из множества
        if (cuisines.contains((String) b.getText())) {
            cuisines.remove((String) b.getText());
            changeButtonState(b, false);
        }
        //Если кнопка нажата впервые, хотим добавить кухню
        else {
            cuisines.add((String) b.getText());
            changeButtonState(b, true);
        }
    }

    private void changeButtonState(Button b, boolean activate) {
        if (activate) {
            b.setTextColor(getResources().getColor(R.color.colorPrimary));
            b.setBackground(getResources().getDrawable(R.drawable.btn_bg_filled));
        } else {
            b.setTextColor(getResources().getColor(R.color.colorWhite));
            b.setBackground(getResources().getDrawable(R.drawable.btn_bg_contour));
        }
    }

    private void initButtons() {
        View rootView = findViewById(R.id.cuisine_choice);
        ArrayList<View> touchables = rootView.getTouchables();
        for (View v : touchables) {
            if (v instanceof Button && cuisines.contains(((Button) v).getText().toString())) {
                changeButtonState((Button) v, true);
            }
        }
    }
}
