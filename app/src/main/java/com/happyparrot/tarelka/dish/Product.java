package com.happyparrot.tarelka.dish;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

@JsonAutoDetect
public class Product {
    private String name;
    private double amount; //Если количество равно 0 => unit = по вкусу => не добавляем в корзину
    private String unit;
    @JsonIgnore
    private transient boolean checked;    //required for frontend purposes

    Boolean addToCart;

    public Product() {

    }

    public Product(String n, double am, String un) {
        name = n;
        amount = am;
        unit = un;
    }

    public double getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    @JsonIgnore
    public boolean getChecked() {
        return checked;
    }

    @JsonIgnore
    public void setChecked(boolean isChecked) {
        checked = isChecked;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(Boolean addToCart) {
        this.addToCart = addToCart;
    }
}
