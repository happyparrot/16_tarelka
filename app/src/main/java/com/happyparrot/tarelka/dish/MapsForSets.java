package com.happyparrot.tarelka.dish;

import java.util.HashMap;
import java.util.Map;

public class MapsForSets {

    public static Map<String, Cuisine> cuis = new HashMap<String, Cuisine>() {{
        put("Русская кухня", Cuisine.RUSSIAN);
        put("Японская кухня", Cuisine.JAPANESE);
        put("Грузинская кухня", Cuisine.GEORGIAN);
        put("Домашняя кухня", Cuisine.HOMEMADE);
        put("Украинская кухня", Cuisine.UKRAINIAN);
    }};

    public static Map<Cuisine, String> cuisReverse = new HashMap<Cuisine, String>() {{
        put(Cuisine.RUSSIAN, "Русская кухня");
        put(Cuisine.JAPANESE, "Японская кухня");
        put(Cuisine.GEORGIAN, "Грузинская кухня");
        put(Cuisine.HOMEMADE, "Домашняя кухня");
        put(Cuisine.UKRAINIAN, "Украинская кухня");
    }};

    public static Map<String, Diet> diet = new HashMap<String, Diet>() {{
        put("Правильное питание", Diet.CHILDREN);
        put("Диетическое", Diet.DIET);
        put("Низкокалорийное", Diet.HEALTHY);
        put("Постное", Diet.POST);
        put("Вегетарианское", Diet.VEGAN);
    }};

    public static Map<Diet, String> diet_reverse = new HashMap<Diet, String>() {{
        put(Diet.CHILDREN, "Правильное питание");
        put(Diet.DIET, "Диетическое");
        put(Diet.HEALTHY, "Низкокалорийное");
        put(Diet.POST, "Постное");
        put(Diet.VEGAN, "Вегетарианское");
    }};

    public static Map<String, Period> per = new HashMap<String, Period>() {{
        put("Завтрак", Period.BREAKFAST);
        put("Обед", Period.DINNER);
        put("Ужин", Period.SUPPER);
        put("Вторые блюда", Period.SUPPER);
        put("Первые блюда", Period.DINNER);
    }};
}
