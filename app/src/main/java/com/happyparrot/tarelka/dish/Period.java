package com.happyparrot.tarelka.dish;

public enum Period {
    BREAKFAST,
    DINNER,
    SUPPER
}
