package com.happyparrot.tarelka.dish;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.EnumSet;

@JsonAutoDetect
public class Dish {
    String name;

    PFCC pffc;
    String timeOfCooking;

    ArrayList<Product> ingredients;
    ArrayList<String> instructions;

    String imageUrl;
    String imagePath;

    EnumSet<Cuisine> cuisine;
    EnumSet<Diet> diet;
    EnumSet<Period> period;


    public String getName() {
        return name;
    }

    public ArrayList<Product> getIngredients() {
        return ingredients;
    }

    public ArrayList<String> getInstructions() {
        return instructions;
    }

    @JsonIgnore
    public String getRecipe() {
        StringBuilder result = new StringBuilder();
        for (String s : instructions) {
            int i = s.indexOf(' ');
            s = s.substring(0, i) + "." + s.substring(i);
            result.append(s).append("\n");
        }
        return result.toString();
    }

    public String getImagePath() {
        return imagePath;
    }

    public EnumSet<Cuisine> getCuisine() {
        return cuisine;
    }

    public EnumSet<Period> getPeriod() {
        return period;
    }

    public EnumSet<Diet> getDiet() {
        return diet;
    }

    public String getTimeOfCooking() {
        return timeOfCooking;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PFCC getPffc() {
        return pffc;
    }

    public void setPffc(PFCC pffc) {
        this.pffc = pffc;
    }

    public void setTimeOfCooking(String timeOfCooking) {
        this.timeOfCooking = timeOfCooking;
    }

    public void setIngredients(ArrayList<Product> ingredients) {
        this.ingredients = ingredients;
    }

    public void setInstructions(ArrayList<String> instructions) {
        this.instructions = instructions;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setCuisine(EnumSet<Cuisine> cuisine) {
        this.cuisine = cuisine;
    }

    public void setDiet(EnumSet<Diet> diet) {
        this.diet = diet;
    }

    public void setPeriod(EnumSet<Period> period) {
        this.period = period;
    }
}
