package com.happyparrot.tarelka.dish;

/*
Класс, который хранит в себе один рецепт.
 */
public enum Cuisine {
    RUSSIAN,
    JAPANESE,
    HOMEMADE,
    GEORGIAN, //грузинская
    UKRAINIAN
}
