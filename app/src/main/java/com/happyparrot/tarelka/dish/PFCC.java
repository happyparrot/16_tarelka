package com.happyparrot.tarelka.dish;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.Serializable;

@JsonAutoDetect
public class PFCC {
    float proteins, fats, carbs;
    int calories;

    public PFCC(float proteins, float fats, float carbs, int calories) {
        this.proteins = proteins;
        this.fats = fats;
        this.carbs = carbs;
        this.calories = calories;
    }

    public PFCC() {}

    public float getCarbs() {
        return carbs;
    }

    public float getFats() {
        return fats;
    }

    public float getProteins() {
        return proteins;
    }

    public int getCalories() {
        return calories;
    }

    public static PFCC sum(PFCC a, PFCC b) {
        return new PFCC(a.getProteins() + b.getProteins(), a.getFats() + b.getFats(), a.getCarbs() + b.getCarbs(), a.getCalories() + b.getCalories());
    }

    public void setProteins(float proteins) {
        this.proteins = proteins;
    }

    public void setFats(float fats) {
        this.fats = fats;
    }

    public void setCarbs(float carbs) {
        this.carbs = carbs;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }
}
