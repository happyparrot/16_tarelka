package com.happyparrot.tarelka;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.happyparrot.tarelka.dish.Dish;
import com.happyparrot.tarelka.menu.CantCreateMenuException;
import com.happyparrot.tarelka.menu.MenuController;
import com.happyparrot.tarelka.menu.OneDayMenu;
import com.happyparrot.tarelka.user.Profile;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    static boolean APP_START = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (APP_START)
            try {
                MenuController.initialize(getDishes());
            } catch (CantCreateMenuException e) {
                e.printStackTrace();
            }
        else
            try {
                MenuController.updateMenu();
            } catch (CantCreateMenuException e) {
                e.printStackTrace();
            }

        if (APP_START)
            displayWelcomeScreen();
        else
            displayMainActivity();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        APP_START = true;
    }

    //welcome
    private void displayWelcomeScreen() {
        setContentView(R.layout.activity_welcome);
        TextView welcomePhrase = findViewById(R.id.textView_welcome);
        SpannableString welcomeText = new SpannableString(welcomePhrase.getText());
        welcomeText.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorGreenDark)), welcomeText.length() - 4, welcomeText.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        welcomePhrase.setText(welcomeText);
    }

    //main screen
    private void displayMainActivity() {
        setContentView(R.layout.activity_main);
        //bottom navigation
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_menu);
        bottomNavigationView.setOnNavigationItemSelectedListener(navigationListener);
        bottomNavigationView.setSelectedItemId(R.id.navigation_home);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navigationListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;
            int id = menuItem.getItemId();
            switch (id) {
                case R.id.navigation_home:
                    selectedFragment = new HomeFragment();
                    break;
                case R.id.navigation_cart:
                    selectedFragment = new CartFragment();
                    break;
                case R.id.navigation_preferences:
                    selectedFragment = new PreferencesFragment();
                    break;
                case R.id.navigation_profile:
                    selectedFragment = new ProfileFragment();
                    break;
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
            return true;
        }
    };

    //метод от пирти для чтения рецептиков из файла
    private List<Dish> getDishes() {
        List<Dish> dishes = new ArrayList<>();
        String string = "";
        InputStream inputStream = null;
        try {
            inputStream = getAssets().open("dishes.txt");
            String result = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            StringReader sr = new StringReader(result);
            ObjectMapper mapper = new ObjectMapper();
            dishes = mapper.readValue(sr, new TypeReference<List<Dish>>() {
            });
            sr.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dishes;
    }

    public void closeActivity(View view) {
        finish();
    }

    public void choosePreferences(View view) {
        APP_START = false;
        startActivity(new Intent(MainActivity.this, CuisineChoiceActivity.class));
    }
}
