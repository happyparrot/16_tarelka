package com.happyparrot.tarelka;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.happyparrot.tarelka.dish.PFCC;
import com.happyparrot.tarelka.dish.Product;
import com.happyparrot.tarelka.menu.MenuController;

public class RecipeActivity extends AppCompatActivity {

    ArrayList<Product> productArrayList = new ArrayList<>();

    //Recipe
    private ImageView recipe_image;
    private TextView recipe_name;
    private TextView recipe_proteins;
    private TextView recipe_fats;
    private TextView recipe_carbohydrates;
    private TextView recipe_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        recipe_name = findViewById(R.id.recipe_name);
        recipe_image = findViewById(R.id.recipe_image);
        recipe_proteins = findViewById(R.id.recipe_proteins_value);
        recipe_fats = findViewById(R.id.recipe_fats_value);
        recipe_carbohydrates = findViewById(R.id.recipe_carbohydrates_value);
        recipe_time = findViewById(R.id.recipe_time);

        Button prepare = findViewById(R.id.prepare);
        ListView ingredients = findViewById(R.id.prepare_recipe_ingredients);

        prepare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(RecipeActivity.this, PrepareRecipeActivity.class));
            }
        });

        fillSampleArrayList();
        IngredientsListAdapter adapter = new IngredientsListAdapter(this, R.layout.listview_ingredients, productArrayList);
        ingredients.setAdapter(adapter);
    }

    void fillSampleArrayList() {

        productArrayList = new ArrayList<>();
        PFCC pfcc;
        switch (HomeFragment.MEAL_INDEX) {
            case 0:
                pfcc = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getPffc();
                recipe_proteins.setText(Float.toString(pfcc.getProteins()));
                recipe_fats.setText(Float.toString(pfcc.getFats()));
                recipe_carbohydrates.setText(Float.toString(pfcc.getCarbs()));
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getName());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getBreakfast().getIngredients();
                break;
            case 1:
                pfcc = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getPffc();
                recipe_proteins.setText(Float.toString(pfcc.getProteins()));
                recipe_fats.setText(Float.toString(pfcc.getFats()));
                recipe_carbohydrates.setText(Float.toString(pfcc.getCarbs()));
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getName());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getDinner().getIngredients();
                break;
            case 2:
                pfcc = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getPffc();
                recipe_proteins.setText(Float.toString(pfcc.getProteins()));
                recipe_fats.setText(Float.toString(pfcc.getFats()));
                recipe_carbohydrates.setText(Float.toString(pfcc.getCarbs()));
                recipe_time.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getTimeOfCooking());
                recipe_name.setText(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getName());
                recipe_image.setImageBitmap(getBitmapFromAssets(MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getImagePath()));
                productArrayList = MenuController.getMenuForDay(HomeFragment.CURRENT_DAY_INDEX).getSupper().getIngredients();
                break;
        }
    }

    private Bitmap getBitmapFromAssets(String fileName) {
        AssetManager am = getAssets();
        InputStream is = null;
        try {
            is = am.open("images/" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(is);
        return bitmap;
    }

    public void closeActivity(View view) {
        finish();
    }
}
