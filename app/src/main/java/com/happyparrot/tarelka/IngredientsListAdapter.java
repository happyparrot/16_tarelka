package com.happyparrot.tarelka;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import com.happyparrot.tarelka.dish.Product;

public class IngredientsListAdapter extends ArrayAdapter<Product> {
    Context context;
    int resource;

    public IngredientsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Product> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(resource, parent, false);

        CheckBox name = convertView.findViewById(R.id.ingredient_name);
        TextView count = convertView.findViewById(R.id.ingredient_count);

        name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getItem(position).setChecked(isChecked);
            }
        });

        name.setText(getItem(position).getName());
        name.setChecked(getItem(position).getChecked());
        count.setText((getItem(position).getAmount() == 0 ? "" : (getItem(position).getAmount() + " ")) + getItem(position).getUnit());

        return convertView;
    }
}
