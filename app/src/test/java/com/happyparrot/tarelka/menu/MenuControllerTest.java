package com.happyparrot.tarelka.menu;

import com.happyparrot.tarelka.dish.*;
import com.happyparrot.tarelka.user.Profile;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

import static org.junit.Assert.*;

public class MenuControllerTest {
    List<Dish> dishes = new ArrayList<>();
    ArrayList<Product> products = new ArrayList<>();

    @Before
    public void setUpgetMenuForWeek() {
        products.add(new Product("morkovka", 1, "kg"));
        for (int i = 0; i < 14; ++i) {
            Dish breakfast = new Dish();
            breakfast.setName(Integer.toString(i));
            breakfast.setPeriod(EnumSet.of(Period.BREAKFAST));
            Dish dinner = new Dish();
            dinner.setName(Integer.toString(i + 28));
            dinner.setPeriod(EnumSet.of(Period.DINNER));
            Dish supper = new Dish();
            supper.setName(Integer.toString(i + 14));
            supper.setPeriod(EnumSet.of(Period.SUPPER));

            breakfast.setIngredients(products);
            supper.setIngredients(products);
            dinner.setIngredients(products);
            breakfast.setCuisine(EnumSet.of(Cuisine.HOMEMADE));
            dinner.setCuisine(EnumSet.of(Cuisine.HOMEMADE));
            supper.setCuisine(EnumSet.of(Cuisine.HOMEMADE));
            breakfast.setDiet(EnumSet.of(Diet.DIET));
            dinner.setDiet(EnumSet.of(Diet.DIET));
            supper.setDiet(EnumSet.of(Diet.DIET));
            breakfast.setPffc(new PFCC(1, 1, 1, 1));
            dinner.setPffc(new PFCC(1, 1, 1, 1));
            supper.setPffc(new PFCC(1, 1, 1, 1));

            Profile.setCuisine(EnumSet.of(Cuisine.HOMEMADE));
            Profile.setDiet(EnumSet.of(Diet.DIET));

            dishes.add(breakfast);
            dishes.add(dinner);
            dishes.add(supper);
        }
    }

    @Test
    public void getMenuForWeek() throws CantCreateMenuException {
        assert dishes.size() == 14 * 3;
        MenuForWeek menu = new MenuForWeek(dishes);
        assert menu.oneDayMenus.size() == 14;
    }

    @Test
    public void removeNonLikedProducts() throws CantCreateMenuException {
        ArrayList<Product> deprecatedProducts = new ArrayList<>();
        Product union = new Product("лучок", 1, "kg");
        ArrayList<Dish> allDishes = new ArrayList<>(dishes);
        deprecatedProducts.add(union);
        for (Dish dish: allDishes) {
            if (Integer.parseInt(dish.getName()) < 12) {
                dish.setIngredients(deprecatedProducts);
                assert dish.getIngredients().get(0).getName() == "лучок";
            }
        }

        Profile.setIngredients(new ArrayList<String>(Arrays.asList("лучок")));
        assert allDishes.size() == 14 * 3;
        MenuForWeek menu = new MenuForWeek(allDishes);

        for (OneDayMenu day: menu.getOneDayMenus()) {
            assert day.getBreakfast().getIngredients().get(0).getName() != "лучок";
        }
    }

    @Test
    public void returnDish() throws CantCreateMenuException {
        MenuForWeek menu = new MenuForWeek(dishes);
        Dish dish = new Dish();
        dish.setName("Kotletka with pureshka");
        dish.setPeriod(EnumSet.of(Period.DINNER));
        dish.setIngredients(products);
        dish.setCuisine(EnumSet.of(Cuisine.HOMEMADE));
        dish.setDiet(EnumSet.of(Diet.DIET));

        ArrayList<Product> ingredients = new ArrayList<>();
        ingredients.add(new Product("kotletka", 1, "штука"));
        ingredients.add(new Product("pureshka", 1, "portion"));

        dish.setIngredients(ingredients);
        menu.returnDish(dish);
        assert menu.dinners.contains(dish);
        assertFalse(menu.breakfasts.contains(dish));
        assertFalse(menu.suppers.contains(dish));
    }

    @Test
    public void getWeeklyPFCC() throws CantCreateMenuException {
        MenuForWeek menu = new MenuForWeek(dishes);
        PFCC cur = menu.getWeeklyPFCC();
        assert cur.getCalories() == 42;
        assert cur.getCarbs() == 42;
        assert cur.getFats() == 42;
        assert cur.getProteins() == 42;
    }

    @Test
    public void getMenuForDay() throws CantCreateMenuException {
        MenuController.initialize(dishes);
        MenuController controller = new MenuController();
        assert dishes.contains(controller.getMenuForDay(1).breakfast);
        assert dishes.contains(controller.getMenuForDay(1).dinner);
        assert dishes.contains(controller.getMenuForDay(1).supper);
    }

    @Test
    public void getMenuForWeekTest() throws CantCreateMenuException {
        MenuController.initialize(dishes);
        MenuController controller = new MenuController();
        assert MenuController.getMenuForWeek().size() == 7;
    }
}