package com.happyparrot.tarelka.dish;

import org.junit.Test;

import java.util.ArrayList;
import java.util.EnumSet;

import static org.junit.Assert.*;

public class DishTest {

    @Test
    public void getName() {
        Dish dish = new Dish();
        dish.setName("Pelmeni");
        assert dish.getName().equals("Pelmeni");
    }

    @Test
    public void getIngredients() {
        Dish dish = new Dish();
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Product("solt", 1, "po vkusu"));
        dish.setIngredients(products);
        assert dish.getIngredients().size() == 1;
        assert dish.getIngredients() == products;
    }

    @Test
    public void getInstructions() {
        Dish dish = new Dish();
        ArrayList<String> instruction = new ArrayList<String>();
        instruction.add("posolit");
        instruction.add("poperchit");
        instruction.add("pozharit");
        dish.setInstructions(instruction);
        assert dish.getInstructions().size() == 3;
        assert dish.getInstructions() == instruction;
    }

    @Test
    public void getImagePath() {
        Dish dish = new Dish();
        dish.setImagePath("awesome/path");
        assertEquals(dish.getImagePath(), "awesome/path");
    }

    @Test
    public void getCuisine() {
        Dish dish = new Dish();
        EnumSet<Cuisine> cuisines = EnumSet.of(Cuisine.HOMEMADE);
        dish.setCuisine(cuisines);
        assert dish.getCuisine().size() == 1;
        assertEquals(cuisines, dish.getCuisine());
    }

    @Test
    public void getPeriod() {
        Dish dish = new Dish();
        EnumSet<Period> period = EnumSet.of(Period.BREAKFAST, Period.SUPPER);
        dish.setPeriod(period);
        assert dish.getPeriod().size() == 2;
        assertEquals(period, dish.getPeriod());
    }

    @Test
    public void getDiet() {
        Dish dish = new Dish();
        dish.setDiet(EnumSet.of(Diet.HEALTHY));
        assertEquals(1, dish.getDiet().size());
        assertEquals(EnumSet.of(Diet.HEALTHY), dish.getDiet());
    }

    @Test
    public void getTimeOfCooking() {
        Dish dish = new Dish();
        dish.setTimeOfCooking("15 nanosecund");
        assertEquals("15 nanosecund", dish.getTimeOfCooking());
    }

    @Test
    public void setName() {
        Dish dish = new Dish();
        assert dish.getName() == null;
        dish.setName("Vine");
        assert dish.getName().equals("Vine");
    }

    @Test
    public void getPffc() {
        Dish dish = new Dish();
        dish.setPffc(new PFCC(1, 1, 1, 1));
        assertEquals(1, dish.getPffc().getCalories());
        assert 1 == dish.getPffc().getFats();
        assert 1 == dish.getPffc().getCarbs();
        assert 1 == dish.getPffc().getProteins();
    }

    @Test
    public void setPffc() {
        Dish dish = new Dish();
        assert dish.getPffc() == null;
        dish.setPffc(new PFCC(3, 1, 100, 4));
        assertEquals(4, dish.getPffc().getCalories());
        assert 1 == dish.getPffc().getFats();
        assert 100 == dish.getPffc().getCarbs();
        assert 3 == dish.getPffc().getProteins();
    }

    @Test
    public void setTimeOfCooking() {
        Dish dish = new Dish();
        assert dish.getTimeOfCooking() == null;
        dish.setTimeOfCooking("very bistro");
        assertEquals("very bistro", dish.getTimeOfCooking());
    }

    @Test
    public void setIngredients() {
        Dish dish = new Dish();
        assert dish.getIngredients() == null;
        ArrayList<Product> products = new ArrayList<Product>();
        products.add(new Product("peper", 1, "po vkusu"));
        dish.setIngredients(products);
        assert dish.getIngredients() == products;
    }

    @Test
    public void setInstructions() {
        Dish dish = new Dish();
        assert dish.getInstructions() == null;
        ArrayList<String> instruction = new ArrayList<String>();
        instruction.add("posolit");
        dish.setInstructions(instruction);
        assert dish.getInstructions() == instruction;
    }

    @Test
    public void getImageUrl() {
        Dish dish = new Dish();
        dish.setImageUrl("http best url . com!!!");
        assertEquals("http best url . com!!!", dish.getImageUrl());
    }

    @Test
    public void setImageUrl() {
        Dish dish = new Dish();
        assert dish.getImageUrl() == null;
        dish.setImageUrl("http://picture.com/tarelka");
        assertEquals("http://picture.com/tarelka", dish.getImageUrl());
    }

    @Test
    public void setImagePath() {
        Dish dish = new Dish();
        assert dish.getImagePath() == null;
        dish.setImagePath("awesome/path");
        assertEquals(dish.getImagePath(), "awesome/path");
    }

    @Test
    public void setCuisine() {
        Dish dish = new Dish();
        assert dish.getCuisine() == null;
        EnumSet<Cuisine> cuisines = EnumSet.of(Cuisine.JAPANESE);
        dish.setCuisine(cuisines);
        assert dish.getCuisine().size() == 1;
        assertEquals(cuisines, dish.getCuisine());
    }

    @Test
    public void setDiet() {
        Dish dish = new Dish();
        assert dish.getDiet() == null;
        dish.setDiet(EnumSet.of(Diet.CHILDREN));
        assertEquals(1, dish.getDiet().size());
        assertEquals(EnumSet.of(Diet.CHILDREN), dish.getDiet());
    }

    @Test
    public void setPeriod() {
        Dish dish = new Dish();
        assert dish.getPeriod() == null;
        EnumSet<Period> period = EnumSet.of(Period.BREAKFAST, Period.SUPPER);
        dish.setPeriod(period);
        assertEquals(period, dish.getPeriod());
    }
}