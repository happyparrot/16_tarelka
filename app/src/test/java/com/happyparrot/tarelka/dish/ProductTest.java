package com.happyparrot.tarelka.dish;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    @Test
    public void getAmount() {
        Product product = new Product("apple", 1, "kg");
        assert product.getAmount() == 1;
    }

    @Test
    public void getName() {
        Product product = new Product("apple", 1, "kg");
        assertEquals("apple", product.getName());
    }

    @Test
    public void getUnit() {
        Product product = new Product("apple", 1, "kg");
        assertEquals("kg", product.getUnit());
    }
}