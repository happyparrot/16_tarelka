package com.happyparrot.tarelka.dish;

import org.junit.Test;

import static org.junit.Assert.*;

public class PFCCTest {

    @Test
    public void getCarbs() {
        PFCC test = new PFCC(1, 2, 3, 4);
        assert test.getCarbs() == 3;
    }

    @Test
    public void getFats() {
        PFCC test = new PFCC(1, 2, 3, 4);
        assert test.getFats() == 2;
    }

    @Test
    public void getProteins() {
        PFCC test = new PFCC(1, 2, 3, 4);
        assert test.getProteins() == 1;
    }

    @Test
    public void getCalories() {
        PFCC test = new PFCC(1, 2, 3, 4);
        assert test.getCalories() == 4;
    }

    @Test
    public void sum() {
        PFCC a = new PFCC(1, 2, 3, 4);
        PFCC b = new PFCC(5, 6, 7, 8);
        PFCC res = PFCC.sum(a, b);
        assert res.getProteins() == 6;
        assert res.getFats() == 8;
        assert res.getCarbs() == 10;
        assert res.getCalories() == 12;
    }
}